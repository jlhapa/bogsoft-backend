<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Bogsoft\Models\Company;

class CompanyController extends Controller
{
    public function index()
    {
    	$data = Company::all();

    	return response()->json($data);
    }

    public function show(Company $company)
    {
    	return response()->json($company);
    }

    public function store(Company $company, Request $request)
    {
    	//
    }
}
