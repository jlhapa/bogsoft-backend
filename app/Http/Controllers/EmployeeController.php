<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Bogsoft\Models\Employee;

class EmployeeController extends Controller
{
    public function index()
    {
    	$data = Employee::with(['company', 'department'])->get();

        return response()->json($data);
    }

    public function show(Employee $employee)
    {
    	return response()->json($employee);
    }

    public function store(Employee $employee, Request $request)
    {
    	//
    }
}
