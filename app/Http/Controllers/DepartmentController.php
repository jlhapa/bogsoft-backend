<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Bogsoft\Models\Department;

class DepartmentController extends Controller
{
    public function index()
    {
    	$data = Department::with('company')->get();

        return response()->json($data);
    }

    public function show(Department $department)
    {
    	return response()->json($department);
    }

    public function store(Department $department, Request $request)
    {
    	//
    }
}
