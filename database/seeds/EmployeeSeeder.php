<?php

use Illuminate\Database\Seeder;
use Bogsoft\Models\Employee;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = [
			'name' => 'Lemuel',
			'email' => 'jl.g.hapa@gmail.com',
			'number' => '09178819970',
			'company_id' => 1,
			'department_id' => 1
		];

	    Employee::query()->firstOrCreate($employee);
    }
}
