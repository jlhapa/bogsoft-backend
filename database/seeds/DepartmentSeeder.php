<?php

use Illuminate\Database\Seeder;
use Bogsoft\Models\Department;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = [
			'name' => 'IT',
			'company_id' => 1,
		];

	    Department::query()->firstOrCreate($department);
    }
}
