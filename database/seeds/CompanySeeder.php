<?php

use Illuminate\Database\Seeder;
use Bogsoft\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = [
			'name' => 'Cashbee',
			'description' => 'IT Company',
		];

	    Company::query()->firstOrCreate($company);
    }
}
