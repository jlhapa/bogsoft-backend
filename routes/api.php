<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('companies')->as('companies.')->group(function () {
	Route::get('', [CompanyController::class, 'index'])->name('index');
	Route::get('{company}', [CompanyController::class, 'show'])->name('show');
	Route::post('{company}', [CompanyController::class, 'store'])->name('store');
});

Route::prefix('departments')->as('departments.')->group(function () {
	Route::get('', [DepartmentController::class, 'index'])->name('index');
	Route::get('{department}', [DepartmentController::class, 'show'])->name('show');
	Route::post('{department}', [DepartmentController::class, 'store'])->name('store');
});

Route::prefix('employees')->as('employees.')->group(function () {
	Route::get('', [EmployeeController::class, 'index'])->name('index');
	Route::get('{employee}', [EmployeeController::class, 'show'])->name('show');
	Route::post('{employee}', [EmployeeController::class, 'store'])->name('store');
});
