<?php

namespace Bogsoft\Services;

use Bogsoft\Services\Notifier;
use Bogsoft\Services\Models\Employee;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmployeeNotified;

class EmailNotifier implements Notifier
{
    private $employee;
    
    public function __contruct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function notify(string $content): void
    {
        Mail::to($employee->email)
            ->from(env('MAIL_FROM_ADDRESS'))
            ->send(new EmployeeNotified($content));
    }
}
