<?php

namespace Bogsoft\Services;

interface Notifier
{
    public function notify(string $content): void;
}
