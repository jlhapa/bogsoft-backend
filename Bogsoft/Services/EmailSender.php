<?php

namespace Bogsoft\Services;

use Bogsoft\Services\MessageSender;
use Bogsoft\Services\Models\Employee;
use Bogsoft\Services\Notifier;
use Bogsoft\Services\EmailNotifier;

class EmailSender extends MessageSender
{
    private $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function getNotification(): Notifier
    {
        return new EmailNotifier($this->employee);
    }
}
