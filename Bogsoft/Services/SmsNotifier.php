<?php

namespace Bogsoft\Services;

use Bogsoft\Services\Notifier;
use Bogsoft\Services\Models\Employee;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SmsNotifier implements Notifier
{
    private $employee;
    
    public function __contruct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function notify(string $content): void
    {
        $client = new Client();

        $client->post('https://www.itexmo.com/php_api/api.php', [
            'form_params' => [
                '1' => $this->employee->number,
                '2' => $content,
                '3' => env('ITEXTMO_API_CODE')
            ]
        ]);
    }
}
