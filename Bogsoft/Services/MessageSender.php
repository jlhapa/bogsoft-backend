<?php

namespace Bogsoft\Services;

use Bogsoft\Services\Notifier;

abstract class MessageSender
{
    abstract public function getNotification(): Notifier;

    public function send(string $content): void
    {
        $notifier = $this->getNotification();

        $notifier->notify($content);
    }
}
