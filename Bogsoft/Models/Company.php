<?php

namespace Bogsoft\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    protected $fillable = [
        'name',
        'description'      
    ];

    public function department()
    {
        return $this->hasMany(Department::class);
    }

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
