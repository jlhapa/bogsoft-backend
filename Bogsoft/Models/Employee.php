<?php

namespace Bogsoft\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    protected $fillable = [
        'name',
        'email',
        'number',
        'company_id',
        'department_id',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
