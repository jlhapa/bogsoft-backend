<?php

namespace Bogsoft\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments';

    protected $fillable = [
        'name',
        'company_id'      
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
